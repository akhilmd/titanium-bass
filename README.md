# 4th Sem DBMS-Project #

This file is best viewed on the repository [website](https://bitbucket.org/akhilmd/titanium-bass/overview).

### Authors: ###
* Durga Akhil Mundroy (01FB15ECS097)
* Harsh Garg (01FB15ECS118)
* Ganesh K. (01FB15ECS104)

### Timeline: ###
* Week 1
	* Discussed project structure, tools and backend interface.
	* Made barebones project with fake implementation.
	* Implemented backend for points 1-5 in [outline.pdf](https://bitbucket.org/akhilmd/titanium-bass/raw/8ec031ef0e418c2096602431f84d051d9dd4f51f/outline.pdf).
* Week 2
	* Discussed structure and production rules for parser.
	* Implemented backend for points 6-10 in [outline.pdf](https://bitbucket.org/akhilmd/titanium-bass/raw/8ec031ef0e418c2096602431f84d051d9dd4f51f/outline.pdf).
* Week 3
	* Implemented backend for points 11-15 in [outline.pdf](https://bitbucket.org/akhilmd/titanium-bass/raw/8ec031ef0e418c2096602431f84d051d9dd4f51f/outline.pdf).
	* Implemented parser for points 1-9 in [outline.pdf](https://bitbucket.org/akhilmd/titanium-bass/raw/8ec031ef0e418c2096602431f84d051d9dd4f51f/outline.pdf).
* Week 4
	* Implemented backend for points 16-19 in [outline.pdf](https://bitbucket.org/akhilmd/titanium-bass/raw/8ec031ef0e418c2096602431f84d051d9dd4f51f/outline.pdf).
	* Implemented parser for points 10-19 in [outline.pdf](https://bitbucket.org/akhilmd/titanium-bass/raw/8ec031ef0e418c2096602431f84d051d9dd4f51f/outline.pdf).

### How to contribute to the project: ###

* Fork this repo.
* Create a branch in your Fork.
* Push changes to your branch.
* Once you want others to see and review your changes, create a pull request.
* We'll go through the pull request and merge them into the master project.